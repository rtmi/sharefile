package main

import (
    "bytes"
    "encoding/json"
    "flag"
    "fmt"
    "io/ioutil"
    "log"
    "net/http"
    "os"
    "time"
)

type Spec struct{
    Hostname string
    Username string
    Password string
}

type Config struct{
    Specs []Spec
}

func main() {
    filepathPtr := flag.String("file", "jobs.json", "JSON file path specifying jobs")
    flag.Parse()
    
    jobs, err := ioutil.ReadFile(*filepathPtr)
    if err != nil {
        fmt.Println("Error:", err)
    }
    var conf Config
    err = json.Unmarshal(jobs, &conf)
    if err != nil {
        fmt.Println("Error:", err)
    }

    for _, job := range conf.Specs {
        jmap := sendauth(job)
        qryitems(jmap)
    }
}

func initgloballog() {
    f, err := os.OpenFile("shfile.log", os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
    if err != nil {
        fmt.Printf("error opening file: %v", err)
        os.Exit(1)
    }
    defer f.Close()

    log.SetOutput(f)
}

func sendauth(job Spec) map[string]interface{} {
    initgloballog()

    req := newauthrequest(job)

    start := time.Now()
    client := &http.Client{}
    resp, err := client.Do(req)
    if err != nil {
        panic(err)
    }
    defer resp.Body.Close()
    elapsed := time.Since(start)

    header := fmt.Sprintf("%s", resp.Header);
    addlogentry("response Status: " + resp.Status)
    addlogentry("response Headers: " + header)

    body, _ := ioutil.ReadAll(resp.Body)

    var bucket interface{}
    err = json.Unmarshal(body, &bucket)
    if err != nil {
        panic(err)
    }

    addlogentry("response Body: " + string(body))
    addlogentry("elapsed: " + elapsed.String())
    m := bucket.(map[string]interface{})
    return m
}

func qryitems(fc map[string]interface{}) {
    req := newitemsrequest(fc)
    client := &http.Client{}
    resp, err := client.Do(req)
    if err != nil {
        panic(err)
    }
    defer resp.Body.Close()

    header := fmt.Sprintf("%s", resp.Header);
    addlogentry("response Status: " + resp.Status)
    addlogentry("response Headers: " + header)

    body, _ := ioutil.ReadAll(resp.Body)

    addlogentry("response Body: " + string(body))
}

func newitemsrequest(fc map[string]interface{}) *http.Request {
    t := fc["subdomain"].(string)
    if (t != "github") { //TODO
        panic("JSON did not contain the expected subdomain field value of ")
    }

    sd := fc["subdomain"].(string)
    url := fmt.Sprintf("https://%s.sf-api.com/sf/v3/Items?$expand=Children", sd);

    var jsonStr = []byte(``)
    req, _ := http.NewRequest("GET", url, bytes.NewBuffer(jsonStr))

    at := "Bearer " + fc["access_token"].(string)
    req.Header.Set("Authorization", at)

    return req
}

func newauthrequest(job Spec) *http.Request {
    url := fmt.Sprintf("https://%s/oauth/token", job.Hostname);
    addlogentry("URL: " + url)

    // TODO
    var jsonStr = []byte(`grant_type=password&client_id=&client_secret=&username=mail.com&password=`)
    req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
    req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

    return req
}

func debugtracejson(jsonobj map[string]interface{}) {
    //debug
    for k, v := range jsonobj {
        switch vv := v.(type) {
        case string:
            fmt.Println(k, "is string", vv)
        case int:
            fmt.Println(k, "is int", vv)
        case []interface{}:
            fmt.Println(k, "is an array:")
            for i, u := range vv {
                fmt.Println(i, u)
            }
        default:
            fmt.Println(k, "is of a type I don't know how to handle")
        }
    }
}

func addlogentry(msg string) {
    fmt.Println(msg)
    log.Print(msg)
}
